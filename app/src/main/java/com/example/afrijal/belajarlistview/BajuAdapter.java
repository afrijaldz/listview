package com.example.afrijal.belajarlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by afrijal on 20/07/16.
 */
public class BajuAdapter extends ArrayAdapter {
    private Context context;
    private String[]judul;
    private String[] harga;
    private int[] gambar;

    public BajuAdapter(Context context, String[] judul, String[] harga, int[] gambar) {
        super(context,R.layout.baris, judul);

        this.context=context;
        this.judul = judul;
        this.harga = harga;
        this.gambar = gambar;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view==null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.baris,null);

        }

        ImageView imageView = (ImageView)view.findViewById(R.id.img);
        imageView.setImageResource(gambar[position]);

        TextView textView1 = (TextView)view.findViewById(R.id.judul);
        textView1.setText(judul[position]);

        TextView textView2 = (TextView)view.findViewById(R.id.harga);
        textView2.setText(harga[position]);

        Button btn = (Button)view.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),"anda membeli "+judul[position].toString(), Toast.LENGTH_SHORT).show();

            }
        });
        return view; //yang di return view
    }
}
